package com.example.hacked

class Selection {
    var selections = arrayOf("Yes", "No", "Maybe")

    fun select(): String {
        val randomNum = (0..2).random()
        return selections[randomNum]
    }
}

// for testing
//fun main() {
//    val myselection = selection().select()
//    println(myselection)
//}